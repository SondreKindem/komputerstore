
window.onload = function (){
    console.log("Ready");

    // Get values from localStorage
    this.workBalance = parseInt(localStorage.getItem("workBalance") ?? 0);
    this.bankBalance = parseInt(localStorage.getItem("bankBalance") ?? 0);
    this.activeLoan = JSON.parse(localStorage.getItem("activeLoan"));
    if(this.activeLoan){
        updateLoanText();
    }

    this.laptops = {
        1: {
            name: "Used laptop",
            text: "A lightly used laptop, works well for web browsing and office.",
            price: 1999,
            image: "img/laptop-1.jpg",
            features: [
                "Office productivity",
                "Ergonomics"
            ]
        },
        2: {
            name: "Gaming laptop",
            text: "Ready to play the latest titles such as Call of Duty and Doom.",
            price: 2499,
            image: "img/laptop-2.jpg",
            features: [
                "Gaming looks",
                "Horsepower"
            ]
        },
        3: {
            name: "Another used laptop",
            text: "It might be used, but it has lots of nice stickers!",
            price: 2000,
            image: "img/laptop-3.jpg",
            features: [
                "Very nice stickers",
                "Works"
            ]
        },
        4: {
            name: "Rare laptop",
            text: "A rare laptop.",
            price: 9999,
            image: "img/laptop-4.jpg",
            features: [
                "Very rare"
            ]
        }
    }

    // Set up laptop select
    const laptopSelect = document.getElementById("laptopSelect");
    for(let laptop in laptops){
        const optionElement = document.createElement("option");
        optionElement.innerText = laptops[laptop].name;
        optionElement.value = laptop;
        laptopSelect.appendChild(optionElement);
    }

    this.paidLoans = [];

    // set html
    updateBalanceText();
}

/*
 * Laptop stuff
 */

function selectLaptop(element){
    const laptop = laptops[element.value];
    document.getElementById("laptopName").innerText = laptop.name;
    document.getElementById("laptopDescription").innerText = laptop.text;
    document.getElementById("laptopPriceTag").innerText = laptop.price;
    document.getElementById("laptopImage").src = laptop.image;
    document.getElementById("laptopContainer").classList.remove("is-invisible");
    document.getElementById("buyLaptopBtn").removeAttribute("disabled");
    document.getElementById("buyLaptopBtn").value = element.value;

    // Add features
    document.getElementById("featureList").innerHTML = "";
    for(const feature of laptop.features){
        const listElement = document.createElement("li");
        listElement.innerText = feature;
        document.getElementById("featureList").appendChild(listElement);
    }
}

function buyLaptop(btn){
    const laptop = laptops[btn.getAttribute("value")];
    if(laptop){
        if(this.bankBalance - laptop.price < 0){
            // Show error text if can't afford
            document.getElementById("buyLaptopHelpText").classList.remove("is-hidden");
        }
        else{
            this.bankBalance -= laptop.price;
            updateBalanceText();
            saveChanges();

            document.getElementById("buyLaptopHelpText").classList.add("is-hidden");
            toggleLaptopModal(laptop);
        }
    }
}

function toggleLaptopModal(laptop){
    if(laptop){
        document.getElementById("laptopModalName").innerText = laptop.name;
        document.getElementById("laptopModalDescription").innerText = laptop.text;
        document.getElementById("laptopModalImage").setAttribute("src", laptop.image);
    }
    document.getElementById("laptopModal").classList.toggle("is-active");
}


/*
 * Work stuff
 */

/**
 * Increase the work balance by 100kr
 * Also play a fun animation!
 */
function doWork(btn){
    this.workBalance += 100;
    document.getElementById("workBalance").innerText = this.workBalance;

    // Reset animation with reflow - kinda hacky, but works nicely
    const iconElem = btn.children[0].children[0];
    iconElem.style.animation = 'none';
    iconElem.offsetHeight;
    iconElem.style.animation = null;

    btn.firstElementChild.firstElementChild.classList.add("rotate");
    saveChanges();
    console.log("Did some work, earned 100 moneys");
}

/**
 * Move all money in workBalance to bank
 */
function transferToBank(btn){
    if(btn){
        btn.classList.toggle("is-loading");
        setTimeout(function (){
            btn.classList.toggle("is-loading");
        }, 500)
    }

    this.bankBalance += this.workBalance;
    this.workBalance = 0;
    updateBalanceText();
    saveChanges();
    console.log("Transferred money to bank");
}

/**
 * Update the text of balance display elements
 */
function updateBalanceText(){
    document.getElementById("workBalance").innerText = this.workBalance;
    document.getElementById("bankBalance").innerText = this.bankBalance;
}

/**
 * Save the work and bank balance to localStorage
 * @returns {boolean} false if values could not be saved
 */
function saveChanges(){
    try{
        localStorage.setItem("workBalance", workBalance.toString());
        localStorage.setItem("bankBalance", bankBalance.toString());
    }
    catch (err){
        return false;
    }
}


/*
 * Loan stuff
 */

function validateLoanAmount(element){
    const helpTextElem = document.getElementById("loanAmountHelpText");

    if(parseInt(element.value) <= this.bankBalance * 2 && !isNaN(parseInt(element.value))){
        element.classList.replace("is-danger", "is-primary");
        helpTextElem.classList.add("is-invisible");
        return true;
    }
    else{
        element.classList.replace("is-primary", "is-danger");
        helpTextElem.classList.remove("is-invisible");
        return false;
    }
}

function updateLoanText(){
    const loanContainer = document.getElementById("loanContainer");
    loanContainer.children[0].textContent = new Date(this.activeLoan.date).toLocaleDateString("no-NO") + ": " + this.activeLoan.amount  + " kr";
    loanContainer.classList.remove("is-hidden");
    document.getElementById("takeLoanBtn").setAttribute("disabled", null);
}

function takeLoan(){
    const loanInput = document.getElementById("loanAmount");
    if(validateLoanAmount(loanInput)){
        const loanAmount = parseInt(loanInput.value);
        this.bankBalance += loanAmount;

        const loan = {
            date: Date.now(),
            amount: loanAmount
        }

        // Set and save loan
        this.activeLoan = loan;
        localStorage.setItem("activeLoan", JSON.stringify(loan));

        updateLoanText();
        updateBalanceText();
        saveChanges();
        toggleLoanModal();
    }
}

function payBackLoan(){
    if(this.bankBalance > this.activeLoan.amount){
        this.bankBalance -= this.activeLoan.amount;
        this.activeLoan = null;
        localStorage.setItem("activeLoan", null); // Remove saved loan

        saveChanges(); // Update the saved balance
        updateBalanceText();

        // Reset elements
        document.getElementById("loanContainer").classList.add("is-hidden");
        document.getElementById("takeLoanBtn").removeAttribute("disabled");
    }
}


/*
 * Misc stuff
 */

/**
 * Toggle hamburger menu of navbar
 * @param element the clicked element
 */
function toggleBurger(element){
    const target = document.getElementById(element.dataset.target);
    element.classList.toggle('is-active');
    target.classList.toggle('is-active');
}

function toggleLoanModal(){
    document.getElementById("loanModal").classList.toggle("is-active");
}

function clearSavedValues(){
    this.workBalance = 0;
    this.bankBalance = 0;
    updateBalanceText();
    saveChanges();
}